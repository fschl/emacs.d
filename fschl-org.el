;; Org-mode awesomeness!
(add-hook 'org-mode-hook
          (lambda ()
            (linum-mode -1)
            ))

(setq org-export-coding-system 'utf-8)
;(setcdr (assq 'system org-file-apps-defaults-gnu ) "xdg-open %s")
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cl" 'org-store-link)
(global-set-key (kbd "<f8>") 'org-agenda)
(global-set-key (kbd "<f7>") 'org-cycle-agenda-files)

(setq org-default-notes-file "~/Documents/Org/tasks.org")
(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
              (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "PHONE" "MEETING"))))

(setq org-agenda-files
      (quote ("~/Documents/Org/tasks.org"
              "~/Documents/Org/journal.org"
              "~/Documents/Org/projects.org"
              "~/Documents/Org/watchlist.org"
              "~/Documents/Org/birthdays.org")))

(setq org-use-fast-todo-selection t)
(setq org-treat-S-cursor-todo-selection-as-state-change nil)
(setq org-todo-keyword-faces
      (quote (("TODO" :foreground "red" :weight bold)
              ("NEXT" :foreground "blue" :weight bold)
              ("DONE" :foreground "forest green" :weight bold)
              ("WAITING" :foreground "orange" :weight bold)
              ("HOLD" :foreground "magenta" :weight bold)
              ("CANCELLED" :foreground "forest green" :weight bold)
              ("MEETING" :foreground "forest green" :weight bold)
              ("PHONE" :foreground "forest green" :weight bold))))

(setq org-capture-templates
      '(("t" "todo list item"           ; name
         entry                          ; type
         (file+headline org-default-notes-file "Tasks")
         "* TODO %?\n DEADLINE: %^T")  ; template
        ("T" "todo list item with source" ; name
         entry                          ; type
         (file+headline org-default-notes-file "Tasks")
         "* TODO %?\n %a \n DEALINE: %^T \n %i") ; template

        ("m" "scheduled meeting"        ; name
         entry                          ; type
         (file+headline org-default-notes-file "Tasks")
         "* MEETING %?\n SCHEDULED: %^T\n %a") ; template

        ("p" "phone call"               ; name
         entry                          ; type
         (file+headline org-default-notes-file "Tasks")
         "* PHONE %?\n %i\n %a")        ; template

        ("a" "Articles"
         entry (file+weektree "~/Documents/Org/journal.org")
         "* %? \n%x \n %u\n- $?")
        )
      )

;; clocking
;;(setq org-clock-into-drawer "CLOCKING")

;; web-mode.org, when your html has embedded css & javascript
(require 'web-mode)
(defun fschl-web-mode-hook ()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset 4)
  (setq web-mode-code-indent-offset 4)
  (setq web-mode-script-padding 4)
  (setq web-mode-style-padding 4)
  (setq web-mode-enable-current-element-highlight t)
  ;;(yas-activate-extra-mode 'html-mode)
  )

;; latex beamer presentations
;; http://orgmode.org/worg/exporters/beamer/tutorial.html
;; http://orgmode.org/worg/exporters/beamer/ox-beamer.html
(require 'ox-latex)
(setq org-beamer-frame-level 3)
; TODO add this one
(add-to-list 'org-latex-packages-alist '("" "listings"))
